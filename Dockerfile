FROM amazoncorretto:11
EXPOSE 9001
ADD target/service-registry.jar service-registry.jar
ENTRYPOINT ["java","-jar","/service-registry.jar"]